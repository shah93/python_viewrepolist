** View BitBucket Repo List **


Simple script written to show your BitBucket Repo List
---

## Description

> Written in python with 'html' and 'request' libraries for scraping the site

## Features

> 1.	List Down all of the Repository Name for the logged in User.
> 2.	Shows Owner and LastUpdated Time for the repo.



